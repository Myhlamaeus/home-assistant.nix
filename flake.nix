{
  description = "";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs-channels/nixos-20.03";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  inputs.pre-commit-hooks.url =
    "github:Myhlamaeus/pre-commit-hooks.nix/bd7cea29e61458bc3d29dccad6fb312dc1bc672d";

  outputs = { self, nixpkgs, flake-utils, pre-commit-hooks }:
    {
      nixosModules.home-assistant = import ./modules;
    } // (flake-utils.lib.eachDefaultSystem (system: rec {
      pre-commit-check = pre-commit-hooks.packages.${system}.run {
        src = ./.;
        # If your hooks are intrusive, avoid running on each commit with a default_states like this:
        # default_stages = ["manual" "push"];
        hooks = { nixfmt.enable = true; };
      };

      devShell = nixpkgs.legacyPackages.${system}.mkShell {
        inherit (pre-commit-check) shellHook;
      };
    }));
}
